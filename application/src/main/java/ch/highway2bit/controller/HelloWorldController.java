package ch.highway2bit.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import ch.highway2bit.models.Employee;

@Controller
@RequestMapping("/")
public class HelloWorldController {

	@RequestMapping(value = "/view", method = RequestMethod.GET)
    public ModelAndView showForm() {
        return new ModelAndView("welcome", "employee", new Employee());
    }


	@RequestMapping(value = "/addEmployee", method = RequestMethod.POST)
	public String submit(@ModelAttribute("employee")Employee employee, 
			BindingResult result, ModelMap model) {

		
		model.addAttribute("name", employee.getName());

		return "employeeView";
	}

}