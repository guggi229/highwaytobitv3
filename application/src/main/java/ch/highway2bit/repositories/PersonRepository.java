package ch.highway2bit.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import ch.highway2bit.models.Person;

public interface PersonRepository  extends JpaRepository<Person, Integer>{
	public interface UserRepository
	  extends JpaRepository<Person, Integer> { }
}
